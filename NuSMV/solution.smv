--This is a comment

MODULE main
	VAR
		pallet1		: pallet(robot, 1,1,pallet2);
		pallet2		: pallet(robot, 5,3,pallet1);

		robot 	: robot(1,3, pallet1,pallet2);

	DEFINE

	----------------------------------------------------------------------------------------------------------------
	-- Specifications
	----------------------------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------------------------
	-- It is possible that at a certain point in time, pallet 2 is on top of pallet 1, and at a later point in time, the stack is reversed.

	CTLSPEC EF ((pallet1.x = pallet2.x & pallet1.y = pallet2.y & pallet1.status = dropped & pallet2.status = dropped & pallet1.height = 1 & pallet2.height = 0) -> EF (pallet1.x = pallet2.x & pallet1.y = pallet2.y & pallet1.status = dropped & pallet2.status = dropped & pallet1.height = 0 & pallet2.height = 1) )
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------
	-- Packets can never be on the same height on a square.
	-- Special note : The check if "pallet 1 is on top of pallet 2 and at the same time, pallet 2 is on top of pallet 1" is not possible
	-- in this design.  Hence, we check if pallets never have the same height on a square.

	CTLSPEC AG ((pallet1.x = pallet2.x & pallet1.y = pallet2.y & pallet1.status = dropped & pallet2.status = dropped) -> (pallet1.height != pallet2.height))
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------
	-- Whatever happens, the robot can always pick up pallet 1

	CTLSPEC AG EF (pallet1.status = picked_up)
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------
	-- Whenever the robot and pallet 1 are at the same position, pallet 1 is carried

	CTLSPEC AG ((robot.x = pallet1.x & robot.y = pallet1.y) -> (pallet1.status = picked_up))
	LTLSPEC G ((robot.x = pallet1.x & robot.y = pallet1.y) -> (pallet1.status = picked_up))
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------
	-- If at some point t in time pallet 1 is being carried, and some later time, it is no longer carried by the robot, then the following holds: starting from t, pallet 1 remains being carried until the robot performs the action drop pallet;

	CTLSPEC AG ( (pallet1.status = picked_up & robot.action = drop_pallet) -> AX (pallet1.status = dropped) )
	CTLSPEC AG ( (pallet1.status = picked_up & robot.action != drop_pallet) -> AX (pallet1.status = picked_up) )
	LTLSPEC G ( (pallet1.status = picked_up & robot.action = drop_pallet) -> X (pallet1.status = dropped) )
	LTLSPEC G ( (pallet1.status = picked_up & robot.action != drop_pallet) -> X (pallet1.status = picked_up) )
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------
	-- If the robot carries pallet 1 infinitely many times and he carries pallet 2 infinitely many times, then he must also rotate clockwise infinitely many times.

	CTLSPEC  EF (pallet1.status = picked_up -> EF pallet2.status = picked_up ) -> EF (robot.action = rotate_clock)
	----------------------------------------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------

MODULE robot(initX, initY, pallet1, pallet2) --initX and initY are the initial x and y coordinates
	VAR
		x : 0..5;
		y : 0..5;
		orientation : {north, east, south, west};
		action : {moveFront, moveBack, pick_pallet, drop_pallet, rotate_clock};

	ASSIGN

		----------------------------------------------------------------------------------------------------------------
		-- Initiation methods
		----------------------------------------------------------------------------------------------------------------
		init(x) := initX;
		init(y) := initY;
		init(action) := {moveFront};
		init(orientation) := {south};

		----------------------------------------------------------------------------------------------------------------
		-- Basic movement and rotation methods
		----------------------------------------------------------------------------------------------------------------

		next(x) :=
   			case
   				action = moveFront : front_next_x;
     			action = moveBack : back_next_x;
     			TRUE : x;
   			esac;
  		next(y) :=
   			case
   				action = moveFront : front_next_y;
	     		action = moveBack : back_next_y;
	     		TRUE : y;
   			esac;

		next(orientation) :=
			case
				action = rotate_clock : nextOrientation;
				TRUE : orientation;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- To calculate our next action we need to check some conditions.
		-- If a pallet is in front or in the back, if it can drop a pallet, ..
		-- Note: the moveBack action is not included in the following code because validation could not be done.
		-- The code that works with the moveBack action is attached at the bottom of this file.
		----------------------------------------------------------------------------------------------------------------

		next(action) :=
			case
				hasPacket :
					case
						action = moveFront :
							case
								has_packet_in_future_front : {drop_pallet, rotate_clock};
								TRUE : {moveFront, drop_pallet, rotate_clock};
							esac;
						action = rotate_clock :
							case
								has_packet_in_rotate_front : {drop_pallet, rotate_clock};
								TRUE : {moveFront, drop_pallet, rotate_clock};
							esac;
						action = pick_pallet :
							case
								((front_next_x = pallet1.x & front_next_y = pallet1.y & front_next_x = pallet2.x & front_next_y = pallet2.y)) : {drop_pallet, rotate_clock};
								TRUE : {moveFront, drop_pallet, rotate_clock};
							esac;
						action = drop_pallet : {pick_pallet, rotate_clock};
						TRUE : {drop_pallet, rotate_clock};
					esac;
				TRUE :
					case
						action = moveFront :
							case
								has_packet_in_future_front : {pick_pallet, rotate_clock};
								TRUE : {moveFront, rotate_clock};
							esac;
						action = rotate_clock :
							case
								has_packet_in_rotate_front : {pick_pallet, rotate_clock};
								TRUE : {moveFront, rotate_clock};
							esac;
						TRUE :
							case
								action = moveBack : {pick_pallet, rotate_clock};
								action = pick_pallet : {drop_pallet ,rotate_clock};
								action = drop_pallet : {pick_pallet, rotate_clock};
							esac;
					esac;
			esac;

	DEFINE

		----------------------------------------------------------------------------------------------------------------
		-- General methods
		----------------------------------------------------------------------------------------------------------------

		nextOrientation :=
			case
				orientation = north : east;
				orientation = east : south;
				orientation = south : west;
				orientation = west : north;
				TRUE : orientation;
			esac;

		getNextOppositeOrientation :=
			case
				orientation = north : west;
				orientation = east : north;
				orientation = south : east;
				orientation = west : south;
				TRUE : orientation;
			esac;

		hasPacket :=
			case
				action = pick_pallet : TRUE;
				pallet1.status = picked_up : TRUE;
				pallet2.status = picked_up : TRUE;
				TRUE : FALSE;
			esac;


		----------------------------------------------------------------------------------------------------------------
		-- Method that returns true if, after this rotation, we have a packet in front of the robot.
		----------------------------------------------------------------------------------------------------------------
		has_packet_in_rotate_front :=
			case
				rotate_next_x = pallet1.x & rotate_next_y = pallet1.y : TRUE;
				rotate_next_x = pallet2.x & rotate_next_y = pallet2.y : TRUE;
				TRUE : FALSE;
			esac;

		rotate_next_x :=
			case
				nextOrientation = east & x < 5: (x + 1) ;
   				nextOrientation = east & x = 5: 0 ;
     			nextOrientation = west & x > 0 : (x - 1) ;
     			nextOrientation = west & x = 0 : 5 ;
     			TRUE : x ;
			esac;

		rotate_next_y :=
			case
				nextOrientation = south & y = 0: 5 ;
	   			nextOrientation = south & y > 0: (y - 1) ;
	   			nextOrientation = north & y = 5 : 0 ;
	     		nextOrientation = north & y < 5 : (y + 1) ;
	     		TRUE : y ;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- Method that returns true if, after this rotation, we have a packet in the back of the robot.
		----------------------------------------------------------------------------------------------------------------
		has_packet_in_rotate_back :=
			case
				rotate_next_x_back = pallet1.x & rotate_next_y_back = pallet1.y : TRUE;
				rotate_next_x_back = pallet2.x & rotate_next_y_back = pallet2.y : TRUE;
				TRUE : FALSE;
			esac;

		rotate_next_x_back :=
			case
				getNextOppositeOrientation = east & x < 5: (x + 1) ;
   				getNextOppositeOrientation = east & x = 5: 0 ;
     			getNextOppositeOrientation = west & x > 0 : (x - 1) ;
     			getNextOppositeOrientation = west & x = 0 : 5 ;
     			TRUE : x ;
			esac;

		rotate_next_y_back :=
			case
				getNextOppositeOrientation = south & y = 0: 5 ;
	   			getNextOppositeOrientation = south & y > 0: (y - 1) ;
	   			getNextOppositeOrientation = north & y = 5 : 0 ;
	     		getNextOppositeOrientation = north & y < 5 : (y + 1) ;
	     		TRUE : y ;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- Method that returns true if in the next state we have a packet in front of the robot.
		----------------------------------------------------------------------------------------------------------------
		has_packet_in_future_front :=
			case
				x_to_be = pallet1.x & y_to_be = pallet1.y : TRUE;
				x_to_be = pallet2.x & y_to_be = pallet2.y : TRUE;
				TRUE : FALSE;
			esac;

		x_to_be :=
			case
				orientation = east & front_next_x < 5: (front_next_x + 1) ;
   				orientation = east & front_next_x = 5: 0 ;
     			orientation = west & front_next_x > 0 : (front_next_x - 1) ;
 				orientation = west & front_next_x = 0 : 5 ;
 				TRUE : front_next_x ;
			esac;

		y_to_be :=
			case
				orientation = south & front_next_y = 0: 5 ;
	   			orientation = south & front_next_y > 0: (front_next_y - 1) ;
	   			orientation = north & front_next_y = 5 : 0 ;
	     		orientation = north & front_next_y < 5 : (front_next_y + 1) ;
	     		TRUE : front_next_y ;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- Method that returns true if in the next state we have a pallet behind the robot or not.
		----------------------------------------------------------------------------------------------------------------

		has_packet_in_future_back :=
			case
				behind_x_to_be = pallet1.x & behind_y_to_be = pallet1.y : TRUE;
				behind_x_to_be = pallet2.x & behind_y_to_be = pallet2.y : TRUE;
				TRUE : FALSE;
			esac;

		behind_x_to_be :=
			case
				orientation = east & back_next_x < 5: (back_next_x + 1) ;
   				orientation = east & back_next_x = 5: 0 ;
     			orientation = west & back_next_x > 0 : (back_next_x - 1) ;
 				orientation = west & back_next_x = 0 : 5 ;
 				TRUE : back_next_x ;
			esac;

		behind_y_to_be :=
			case
				orientation = south & back_next_y = 0: 5 ;
	   			orientation = south & back_next_y > 0: (back_next_x - 1) ;
	   			orientation = north & back_next_y = 5 : 0 ;
	     		orientation = north & back_next_y < 5 : (back_next_x + 1) ;
	     		TRUE : back_next_y ;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- General direction calculations
		----------------------------------------------------------------------------------------------------------------

		front_next_x :=
			case
				orientation = east & x < 5: (x + 1) ;
   				orientation = east & x = 5: 0 ;
     			orientation = west & x > 0 : (x - 1) ;
     			orientation = west & x = 0 : 5 ;
     			TRUE : x ;
			esac;

		front_next_y :=
			case
				orientation = south & y = 0: 5 ;
	   			orientation = south & y > 0: (y - 1) ;
	   			orientation = north & y = 5 : 0 ;
	     		orientation = north & y < 5 : (y + 1) ;
	     		TRUE : y ;
			esac;

		back_next_x :=
			case
				orientation = east & x > 0: (x - 1) ;
   				orientation = east & x = 0: 5 ;
     			orientation = west & x < 5 : (x + 1) ;
     			orientation = west & x = 5 : 0 ;
     			TRUE : x ;
			esac;

		back_next_y :=
			case
				orientation = south & y = 5: 0 ;
	   			orientation = south & y < 5: (y + 1) ;
	   			orientation = north & y = 0 : 5 ;
	     		orientation = north & y > 0 : (y - 1) ;
	     		TRUE : y ;
			esac;

		----------------------------------------------------------------------------------------------------------------
		-- The working code for the moveBack action on which the verification program keeps getting stuck.
		-- This would be the content of the next(action) method.
		----------------------------------------------------------------------------------------------------------------
		--	hasPacket :
		--			case
		--				action = moveFront :
		--					case
		--						has_packet_in_future_back & has_packet_in_future_front : {drop_pallet, rotate_clock};
		--						! has_packet_in_future_back & has_packet_in_future_front : {moveBack, drop_pallet, rotate_clock};
		--						has_packet_in_future_back & ! has_packet_in_future_front : {moveFront, drop_pallet, rotate_clock};
		--						TRUE : {moveFront, moveBack, drop_pallet, rotate_clock};
		--					esac;
		--				action = moveBack :
		--					case
		--						has_packet_in_future_back & has_packet_in_future_front : {drop_pallet, rotate_clock};
		--						! has_packet_in_future_back & has_packet_in_future_front : {moveBack, drop_pallet, rotate_clock};
		--						has_packet_in_future_back & ! has_packet_in_future_front : {rotate_clock};
		--						TRUE : {moveFront, moveBack, drop_pallet, rotate_clock};
		--					esac;
		--				action = rotate_clock :
		--					case
		--						has_packet_in_future_back & has_packet_in_future_front : {drop_pallet, rotate_clock};
		--						! has_packet_in_future_back & has_packet_in_future_front : {moveBack, drop_pallet, rotate_clock};
		--						has_packet_in_future_back & ! has_packet_in_future_front : {rotate_clock};
		--						TRUE : {moveFront, moveBack, drop_pallet, rotate_clock};
		--					esac;
		--				action = drop_pallet : {pick_pallet, rotate_clock};
		--				TRUE : {moveBack, moveFront, drop_pallet, rotate_clock};
		--			esac;
		--		TRUE :
		--			case
		--				action = moveFront :
		--					case
		--						has_packet_in_future_back & has_packet_in_future_front : {pick_pallet, rotate_clock};
		--						! has_packet_in_future_back & has_packet_in_future_front : {moveBack, pick_pallet, rotate_clock};
		--						has_packet_in_future_back & ! has_packet_in_future_front : {moveFront, rotate_clock};
		--						TRUE : {moveBack, moveFront, rotate_clock};
		--					esac;
		--				action = moveBack :
		--					case
		--						has_packet_in_future_back & has_packet_in_future_front : {pick_pallet, rotate_clock};
		--						! has_packet_in_future_back & has_packet_in_future_front : {moveBack, pick_pallet, rotate_clock};
		--						has_packet_in_future_back & ! has_packet_in_future_front : {moveFront, rotate_clock};
		--						TRUE : {moveBack, moveFront, rotate_clock};
		--					esac;
		--				action = rotate_clock :
		--					case
		--						has_packet_in_rotate_back & has_packet_in_rotate_front : {pick_pallet, rotate_clock};
		--						! has_packet_in_rotate_back & has_packet_in_rotate_front : {moveBack, pick_pallet, rotate_clock};
		--						has_packet_in_rotate_back & ! has_packet_in_rotate_front : {moveFront, rotate_clock};
		--						TRUE : {moveFront, moveBack, rotate_clock};
		--					esac;
		--				TRUE :
		--					case
		--						action = pick_pallet : {drop_pallet ,rotate_clock};
		--						action = drop_pallet : {pick_pallet, rotate_clock};
		--					esac;
		--			esac;
		--	esac;

		----------------------------------------------------------------------------------------------------------------



MODULE pallet(robot, initX, initY, otherPallet) --initX and initY are the initial x and y coordinates
	VAR
		x : 0..5;
		y : 0..5;

		height : 0..1;

		status : {dropped, picked_up};

	ASSIGN

		----------------------------------------------------------------------------------------------------------------
		-- Initiation methods
		----------------------------------------------------------------------------------------------------------------
		init(x) := initX;
		init(y) := initY;

		init(height) := 0;

		init(status) := dropped;

		----------------------------------------------------------------------------------------------------------------
		-- Basic movement and status methods
		----------------------------------------------------------------------------------------------------------------
		next(x) :=
			case
				status = dropped & !hasPalletOnTop & robot.action = pick_pallet & robot.front_next_x = x & robot.front_next_y = y : robot.x;
				status = picked_up & robot.action != drop_pallet: nextrobotx;
				status = picked_up & robot.action = drop_pallet: robot.front_next_x;
				TRUE : x;
			esac;
		next(y) :=
			case
				status = dropped & !hasPalletOnTop & robot.action = pick_pallet & robot.front_next_x = x & robot.front_next_y = y : robot.y;
				status = picked_up & robot.action != drop_pallet: nextroboty;
				status = picked_up & robot.action = drop_pallet: robot.front_next_y;
				TRUE : y;
			esac;

		next(status) :=
			case
				status = dropped :
					case
						hasPalletOnTop : {dropped};
						robot.action = pick_pallet & robot.front_next_x = x & robot.front_next_y = y : {picked_up} ;
						TRUE : {dropped} ;
					esac;
				status = picked_up & robot.action = drop_pallet : {dropped};
				status = picked_up & robot.action != drop_pallet : {picked_up};
				TRUE : {picked_up, dropped};
			esac;

		next(height) :=
			case
				robot.action = drop_pallet & status = picked_up :
					case
						robot.front_next_x = otherPallet.x & robot.front_next_y = otherPallet.y : 1;
						TRUE : 0;
					esac;
				status = picked_up : 0 ;
				TRUE : height;
			esac;
		----------------------------------------------------------------------------------------------------------------

	DEFINE
		----------------------------------------------------------------------------------------------------------------
		-- Method that returns true if there is another packet on top of this packet.
		----------------------------------------------------------------------------------------------------------------
		hasPalletOnTop :=
			case
				otherPallet.x = x & otherPallet.y = y & height < otherPallet.height : TRUE;
				TRUE : FALSE;
			esac;

		nextrobotx :=
   			case
   				robot.action = moveFront : robot.front_next_x;
     			robot.action = moveBack : robot.back_next_x;
     			TRUE : robot.x;
   			esac;
  		nextroboty :=
   			case
   				robot.action = moveFront : robot.front_next_y;
	     		robot.action = moveBack : robot.back_next_y;
	     		TRUE : robot.y;
   			esac;
